(
    app-admin/66-exherbo[~scm]
    app-admin/eclectic[~scm]
    app-admin/s6-exherbo[~scm]
    app-arch/libarchive[~scm]
    app-editors/e4r[~scm]
    app-misc/screen[~scm]
    app-shells/bash-completion[~scm]
    app-shells/zsh[~scm]
    app-text/djvu[~scm]
    app-vim/exheres-syntax[~scm]
    dev-db/xapian-core[~scm]
    dev-lang/clang[~scm]
    dev-lang/llvm[~scm]
    dev-libs/compiler-rt[~scm]
    dev-libs/fmt[~scm]
    dev-libs/oblibs[~scm]
    dev-libs/pinktrace[~scm]
    dev-libs/pinktrace[~0-scm]
    dev-libs/pugixml[~scm]
    dev-util/exherbo-dev-tools[~scm]
    dev-util/ltrace[~scm]
    dev-util/systemtap[~scm]
    dev-util/tig[~scm]
    dev-util/valgrind[~scm]
    net-irc/irssi[~scm]
    net-wireless/iw[~scm]
    net-wireless/wpa_supplicant[~scm]
    net-www/elinks[~scm]
    sys-apps/66[~scm]
    sys-apps/66-tools[~scm]
    sys-apps/dbus[~scm]
    sys-apps/eudev[~scm]
    sys-apps/paludis[~scm]
    sys-apps/multiload[~scm]
    sys-apps/sydbox[~scm]
    sys-apps/systemd[~scm]
    sys-boot/dracut[~scm]
    sys-boot/efibootmgr[~scm]
    sys-devel/lld[~scm]
    sys-devel/lldb[~scm]
    sys-devel/meson[~scm]
    sys-devel/ninja[~scm]
    sys-fs/btrfs-progs[~scm]
    sys-fs/exfatprogs[~scm]
    sys-libs/gcompat[~scm]
    sys-libs/libc++[~scm]
    sys-libs/libc++abi[~scm]
    sys-libs/libsyd[~scm]
    sys-libs/libsydtime[~scm]
    sys-libs/llvm-libunwind[~scm]
    sys-libs/musl[~scm]
    sys-libs/musl-compat[~scm]
    sys-libs/openmp[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

dev-util/ccache [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 14 Apr 2015 ]
    token = broken
    description = [
        UNSUPPORTED: Results in subtle, often undetectable breakage. Don't use it to compile packages or you get to keep both pieces.
    ]
]]

media-libs/jasper[<4.2.3] [[
    author = [ David Legrand <david.legrand@clever-cloud.com> ]
    date = [ 21 Apr 2024 ]
    token = security
    description = [ CVE-2024-31744 ]
]]

net-misc/curl[<8.9.1] [[
    author = [ Timo Gurr <tgur@exherbo.org> ]
    date = [ 01 Aug 2024 ]
    token = security
    description = [ CVE-2024-7264 ]
]]

app-admin/sudo[<1.9.15_p5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Jan 2024 ]
    token = security
    description = [ CVE-2023-42465 ]
]]

(
    media-libs/libpng:1.2[<1.2.57]
    media-libs/libpng:1.5[<1.5.28]
    media-libs/libpng:1.6[<1.6.27]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 03 Jan 2017 ]
    *token = security
    *description = [ CVE-2016-10087 ]
]]

app-arch/libzip[<1.3.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-12858, CVE-2017-14107 ]
]]

dev-libs/expat[<2.6.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Sep 2024 ]
    token = security
    description = [ CVE-2024-45490, CVE-2024-45491, CVE-2024-45492 ]
]]

media-libs/raptor[<2.0.15-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Nov 2020 ]
    token = security
    description = [ CVE-2017-18926 ]
]]

dev-lang/python:2.7[<2.7.18-r2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 Jan 2021 ]
    token = security
    description = [ CVE-2019-20907, CVE-2020-{27619,8492} CVE-2021-3177 ]
]]

(
    dev-lang/python:3.8[<3.8.18]
    dev-lang/python:3.9[<3.9.18]
    dev-lang/python:3.10[<3.10.13]
    dev-lang/python:3.11[<3.11.5]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 25 Aug 2023 ]
    *token = security
    *description = [ CVE-2023-40217 ]
]]

dev-lang/python:3.11[<3.11.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 25 Aug 2023 ]
    token = security
    description = [ CVE-2023-41105 ]
]]

net-libs/libotr[<=3.2.0] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 30 Aug 2012 ]
    token = security
    description = [ CVE-2012-2369 ]
]]

net-print/cups[<2.4.9] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Jun 2024 ]
    token = security
    description = [ CVE-2024-35235 ]
]]

dev-libs/dbus-glib[<0.100.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Mar 2013 ]
    token = security
    description = [ CVE-2013-0292 ]
]]

(
    dev-libs/libxml2[<2.11.8]
    dev-libs/libxml2[>=2.12&<2.12.7]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 16 May 2024 ]
    *token = security
    *description = [ CVE-2024-34459 ]
]]

media-libs/tiff[<4.7.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Sep 2024 ]
    token = security
    description = [ CVE-2023-52356, CVE-2024-7006 ]
]]

sys-apps/dbus[<1.14.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Oct 2022 ]
    token = security
    description = [ CVE-2022-{42010,42011,42012} ]
]]

app-crypt/gnupg[<2.3.7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Jul 2022 ]
    token = security
    description = [ CVE-2022-34903 ]
]]

(
    dev-libs/icu:57.1[<57.1-r1]
    dev-libs/icu:58.1[<58.2-r1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2017-7867, CVE-2017-7868 ]
]]

net-misc/openssh[<9.8_p1] [[
    author = [ Maxime Sorin <maxime.sorin@clever-cloud.com> ]
    date = [ 01 Jul 2024 ]
    token = security
    description = [ CVE-2024-6387 ]
]]

dev-utils/ack[<2.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Dez 2013 ]
    token = security
    description = [ http://beyondgrep.com/security/ ]
]]

sys-apps/file[<5.37-r2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Nov 2019 ]
    token = security
    description = [ CVE-2019-18218 ]
]]

dev-libs/gnutls[<3.8.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Mar 2024 ]
    token = security
    description = [ CVE-2024-28834, CVE-2024-28835 ]
]]

net-print/cups-filters[<1.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Dec 2015 ]
    token = security
    description = [ CVE-2015-8560 ]
]]

app-arch/lzo[<2.07] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jul 2014 ]
    token = security
    description = [ CVE-2014-4607 ]
]]

net-libs/cyrus-sasl[<2.1.28] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Feb 2022 ]
    token = security
    description = [ CVE-2019-19906, CVE-2022-24407 ]
]]

app-crypt/gpgme[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Aug 2014 ]
    token = security
    description = [ CVE-2014-3564 ]
]]

dev-libs/libgcrypt[<1.9.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Jan 2021 ]
    token = security
    description = [ https://dev.gnupg.org/T5275, CVE not yet available ]
]]

app-shells/bash[<4.3_p30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Oct 2014 ]
    token = security
    description = [ CVE-2014-6271, CVE-2014-6277, CVE-2014-6278, CVE-2014-7169, CVE-2014-7186, CVE-2014-7187 ]
]]

net-misc/wget[<1.19.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 07 May 2018 ]
    token = security
    description = [ CVE-2018-0494 ]
]]

dev-libs/libksba[<1.6.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 17 Oct 2022 ]
    token = security
    description = [ CVE-2022-3515 ]
]]

net-dns/bind[<9.18.28] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Jul 2024 ]
    token = security
    description = [ CVE-2024-{0760,1737,1975,4076} ]
]]

net/ntp[<4.2.8_p17] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jun 2023 ]
    token = security
    description = [ CVE-2023-2655{1..5} ]
]]

dev-scm/subversion[<1.10.7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Feb 2021 ]
    token = security
    description = [ CVE-2020-17525 ]
]]

(
    dev-libs/libevent:0[<2.0.22-r3]
    dev-libs/libevent:2.1[<2.1.6]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 23 May 2017 ]
    *token = security
    *description = [ CVE-2016-1019{5,6,7} ]
]]

sys-devel/patch[<2.7.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Feb 2015 ]
    token = security
    description = [ CVE-2015-1196 ]
]]

sys-apps/grep[<2.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Nov 2015 ]
    token = security
    description = [ CVE-2015-1345 ]
]]

sys-libs/glibc[<2.39-r6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Apr 2024 ]
    token = security
    description = [ CVE-2024-33599, CVE-2024-33600, CVE-2024-33601, CVE-2024-33602 ]
]]

sys-fs/e2fsprogs[<1.45.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Sep 2019 ]
    token = security
    description = [ CVE-2019-5094 ]
]]

(
    dev-lang/perl:5.34[<5.34.2]
    dev-lang/perl:5.36[<5.36.2]
    dev-lang/perl:5.38[<5.38.1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 27 Nov 2023 ]
    *token = security
    *description = [ CVE-2023-47038 ]
]]

media-libs/gd[<2.3.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Sep 2020 ]
    token = security
    description = [ CVE-2018-{5711,14553,1000222}, CVE-2019-697{7,8}, CVE-2019-11038 ]
]]

app-arch/libtar[<1.2.20] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2015 ]
    token = security
    description = [ CVE-2013-4397 ]
]]

net-libs/libssh2[<1.11.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Jan 2024 ]
    token = security
    description = [ CVE-2023-48795 ]
]]

dev-libs/libtasn1[<4.12-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Sep 2017 ]
    token = security
    description = [ CVE-2017-10790 ]
]]

sys-apps/paludis[~2.4.0] [[
    author = [ Thomas Anderson <tanderson@caltech.edu> ]
    date = [ 15 Apr 2015 ]
    description = [ Doesn't support multiarch, downgrading from scm *will* break your system ]
    token = broken
]]

dev-libs/pcre[<8.40-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6004 ]
]]

dev-db/sqlite:3[<3.43.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Feb 2024 ]
    token = security
    description = [ CVE-2024-0232 ]
]]

app-antivirus/clamav[<1.4.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Sep 2024 ]
    token = security
    description = [ CVE-2024-20505, CVE-2024-20506 ]
]]

dev-libs/xerces-c[<3.2.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jul 2024 ]
    token = security
    description = [ CVE-2024-23807 ]
]]

(
    sys-fs/fuse:0[<2.9.8]
    sys-fs/fuse:3[<3.2.5]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 25 Jul 2018 ]
    *token = security
    *description = [ CVE-2018-10906 ]
]]

sys-libs/pam[<1.6.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Feb 2024 ]
    token = security
    description = [ CVE-2024-22365 ]
]]

sys-apps/less[<475] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Jul 2015 ]
    token = security
    description = [ CVE-2014-9488 ]
]]

net-dns/libidn[<1.33-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-14062 ]
]]

sys-fs/xfsprogs[<3.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Aug 2015 ]
    token = security
    description = [ CVE-2012-2150 ]
]]

net-nds/rpcbind[<0.2.4-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-8779 ]
]]

sys-apps/xinetd[<2.3.15-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Nov 2015 ]
    token = security
    description = [ CVE-2013-4342 ]
]]

app-arch/unzip[<6.0-r7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Aug 2022 ]
    token = security
    description = [ CVE-2022-0529, CVE-2022-0530 ]
]]

(
    sys-apps/systemd[<249.8]
    sys-apps/systemd[>=250.0&<250.2]
) [[
    *author = [ Heiko <heirecka@exherbo.org> ]
    *date = [ 12 Jan ]
    *token = security
    *description = [ CVE-2021-3997 ]
]]

app-arch/p7zip[<16.02-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Feb 2018 ]
    token = security
    description = [ CVE-2017-17969, CVE-2018-5996 ]
]]

sys-boot/grub[<2.06-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2022 ]
    token = security
    description = [ CVE-2021-3981 ]
]]

dev-libs/libxslt[<1.1.35] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Feb 2022 ]
    token = security
    description = [ CVE-2021-30560 ]
]]

app-misc/screen[<4.9.0-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Jun 2023 ]
    token = security
    description = [ CVE-2023-24626 ]
]]

dev-libs/botan[<2.19.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 16 Nov 2022 ]
    token = security
    description = [ CVE-2022-43705 ]
]]

(
    dev-scm/git[<2.44.1]
    dev-scm/git[>=2.45.0&<2.45.1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 14 May 2024 ]
    *token = security
    *description = [ CVE-2024-{32002,32004,32020,2024-32021} ]
]]

sys-apps/busybox[<1.34.1-r1] [[
    author = [ Lucas Roberto <sigmw@protonmail.com> ]
    date = [ 26 Aug 2022 ]
    token = security
    description = [ CVE-2022-28391 ]
]]

media-gfx/ImageMagick[<6.9.13.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Dec 2023 ]
    token = security
    description = [ CVE-2023-5341, CVE-2023-39978 ]
]]

dev-libs/jansson[<2.7-r1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 05 May 2016 ]
    token = security
    description = [ CVE-2016-4425 ]
]]

net-wireless/wpa_supplicant[<2.10-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Feb 2024 ]
    token = security
    description = [ CVE-2023-52160 ]
]]

net-misc/openntpd[<6.0_p1] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 31 May 2016 ]
    token = security
    description = [ CVE-2016-5117 ]
]]

media-gfx/GraphicsMagick[<1.3.36] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2021 ]
    token = security
    description = [ CVE-2020-12672 ]
]]

app-arch/libarchive[<3.7.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Sep 2024 ]
    token = security
    description = [ CVE-2024-20696, CVE-2024-26256 ]
]]

(
    dev-lang/node[<18.20.4]
    dev-lang/node[>=20&<20.15.1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 09 Jul 2024 ]
    *token = security
    *description = [ CVE-2024-22020, CVE-2024-36138 ]
]]

app-arch/p7zip[<15.14.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Jun 2016 ]
    token = security
    description = [ CVE-2016-2334, CVE-2016-2335 ]
]]

sys-devel/flex[<2.6.1-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Aug 2016 ]
    token = security
    description = [ CVE-2016-6354 ]
]]

app-arch/tar[<1.31] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jan 2018 ]
    token = security
    description = [ CVE-2018-20482 ]
]]

dev-lang/guile:1.8[<1.8.8-r3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Dec 2016 ]
    token = security
    description = [ CVE-2016-8605 ]
]]

dev-lang/guile:2[<2.0.13] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Nov 2016 ]
    token = security
    description = [ CVE-2016-8605 CVE-2016-8606 ]
]]

net-dialup/ppp[<2.4.7-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Nov 2016 ]
    token = security
    description = [ CVE-2015-3310 ]
]]

sys-libs/cracklib[<2.9.6-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Dec 2016 ]
    token = security
    description = [ CVE-2015-6318 ]
]]

sys-libs/musl[<1.1.16] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jan 2017 ]
    token = security
    description = [ CVE-2016-8859 ]
]]

media-libs/jbig2dec[<0.13-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 May 2017 ]
    token = security
    description = [ CVE-2017-{7885,7975,7976,9216} ]
]]

app-text/ghostscript[<10.03.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 May 2024 ]
    token = security
    description = [ CVE-2023-52722, CVE-2024-{29510,33869,33870,33871} ]
]]

sys-fs/ntfs-3g_ntfsprogs[<2022.10.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 31 Oct 2022 ]
    token = security
    description = [ CVE-2022-40284 ]
]]

app-editors/vim[<9.0.2130] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Nov 2023 ]
    token = security
    description = [ CVE-2023-48706 ]
]]

sys-apps/shadow[<4.12.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Aug 2022 ]
    token = security
    description = [ CVE-2013-4235 ]
]]

net-dns/c-ares[<1.27.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Feb 2024 ]
    token = security
    description = [ CVE-2024-25629 ]
]]

net-libs/libtirpc[<1.3.2-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Aug 2022 ]
    token = security
    description = [ CVE-2021-46828 ]
]]

(
    dev-lang/go[<1.22.7]
    dev-lang/go[>=1.23&<1.23.1]
) [[
    *author = [ David Legrand <david.legrand@clever-cloud.com> ]
    *date = [ 05 Sep 2024 ]
    *token = security
    *description = [ CVE-2024-{34155,34156,34158} ]
]]

dev-libs/libbsd[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Feb 2016 ]
    token = security
    description = [ CVE-2016-2090 ]
]]

net-directory/openldap[<2.6.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2022 ]
    token = security
    description = [ CVE-2022-29155 ]
]]

sys-devel/binutils[<2.28-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2017 ]
    token = security
    description = [ CVE-2017-6969, CVE-2017-6966, CVE-2017-6965, CVE-2017-9041, CVE-2017-9040,
                    CVE-2017-9042, CVE-2017-9039, CVE-2017-9038, CVE-2017-8421, CVE-2017-8396,
                    CVE-2017-8397, CVE-2017-8395, CVE-2017-8394, CVE-2017-8393, CVE-2017-8398,
                    CVE-2017-7614 ]
]]

app-arch/unrar[<5.5.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Jun 2017 ]
    token = security
    description = [ CVE-2012-6706 ]
]]

dev-scm/mercurial[<4.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Aug 2017 ]
    token = security
    description = [ CVE-2017-1000115, CVE-2017-1000116 ]
]]

dev-scm/subversion[<1.14.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Apr 2022 ]
    token = security
    description = [ CVE-2021-28544 ]
]]

sys-apps/coreutils[<9.4-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Feb 2024 ]
    token = security
    description = [ CVE-2024-0684 ]
]]

app-text/podofo[<0.9.5_p20170903] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Sep 2017 ]
    token = security
    description = [ CVE-2017-{5852,5853,5854,5855,5886,6840,6844,6847,7378,
                              7379,7380,7994,8787} ]
]]

net-dns/dnsmasq[<2.90] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Feb 2024 ]
    token = security
    description = [ CVE 2023-50387, CVE 2023-50868 ]
]]

mail-mta/exim[<4.98] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jul 2024 ]
    token = security
    description = [ CVE-2024-39929 ]
]]

net-misc/rsync[<3.2.4-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Aug 2022 ]
    token = security
    description = [ CVE-2022-29154 ]
]]

sys-libs/ncurses[<6.3_p20221126] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Dec 2022 ]
    token = security
    description = [ CVE-2022-29458 ]
]]

app-arch/gcab[<1.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Feb 2018 ]
    token = security
    description = [ CVE-2018-5345 ]
]]

net-irc/irssi[<1.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Oct 2020 ]
    token = security
    description = [ CVE-2019-15717 ]
]]

net-dns/idnkit[>=2.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Mar 2018 ]
    token = broken
    description = [ Breaks its solely users bind{,-tools} ]
]]

(
    dev-db/postgresql[<12.20]
    dev-db/postgresql[>=13&<13.16]
    dev-db/postgresql[>=14&<14.13]
    dev-db/postgresql[>=15&<15.8]
    dev-db/postgresql[>=16&<16.4]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 09 Aug 2024 ]
    *token = security
    *description = [ CVE-2024-7348 ]
]]

sys-apps/util-linux[<2.40] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Apr 2024 ]
    token = security
    description = [ CVE-2024-28085 ]
]]

(
    dev-lang/php:8.1[<8.1.29]
    dev-lang/php:8.2[<8.2.20]
    dev-lang/php:8.3[<8.3.8]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 12 Jun 2024 ]
    *token = security
    *description = [ CVE-2024-4577, CVE-2024-5458, CVE-2024-5585 ]
]]

sys-apps/busybox[<1.28.4-r1] [[
    *author = [ Kylie McClain <somasis@exherbo.org> ]
    *date = [ 7 Jun 2018 ]
    *token = security
    *description = [ Using busybox wget is insecure for https urls, as there is
                     no certificate validation done. Busybox >=1.28.4-r1 changes
                     configuration options to disable HTTPS functionality and
                     error when used. ]
]]

sys-process/procps[<4.0.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Sep 2023 ]
    token = security
    description = [ CVE-2023-4016 ]
]]

app-arch/sharutils[<4.15.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Aug 2018 ]
    token = security
    description = [ CVE-2018-1000097 ]
]]

app-arch/cabextract[<1.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Nov 2018 ]
    token = security
    description = [ CVE-2018-18584 ]
]]

dev-util/elfutils[<0.176] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Feb 2019 ]
    token = security
    description = [  CVE-2019-{7146,7148,7149,7150,7664,7665} ]
]]

sys-devel/gettext[<0.19.8.1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jan 2019 ]
    token = security
    description = [ CVE-2018-18751 ]
]]

net-libs/ldns[<1.7.0-rc1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Feb 2019 ]
    token = security
    description = [ CVE-2017-1000231, CVE-2017-1000232 ]
]]

(
    dev-lang/php:5.6
    dev-lang/php:7.0
    dev-lang/php:7.1
    dev-lang/php:7.2
    dev-lang/php:7.3
    dev-lang/php:7.4
    dev-lang/php:8.0
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 25 Aug 2020 ]
    *token = pending-removal
    *description = [ EOL ]
]]

sys-libs/libseccomp[<2.4.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Mar 2019 ]
    token = security
    description = [ https://www.openwall.com/lists/oss-security/2019/03/15/1 ]
]]

sys-process/cronie[<1.5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Apr 2019 ]
    token = security
    description = [ CVE-2019-9704, CVE-2019-9705 ]
]]

dev-libs/zziplib[<0.13.69-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jun 2019 ]
    token = security
    description = [ CVE-2018-16548, CVE-2018-17828 ]
]]

dev-libs/glib:2[<2.78.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 May 2024 ]
    token = security
    description = [ CVE-2024-34397 ]
]]

sys-libs/musl[<1.1.23-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 10 Aug 2019 ]
    token = security
    description = [ CVE-2019-14697 ]
]]

media-gfx/graphviz[<10.0.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Feb 2024 ]
    token = security
    description = [ CVE-2023-46045 ]
]]

(
    dev-lang/ruby:2.7[<2.7.8]
    dev-lang/ruby:3.0[<3.0.7]
    dev-lang/ruby:3.1[<3.1.5]
    dev-lang/ruby:3.2[<3.2.4]
    dev-lang/ruby:3.3[<3.3.1]
) [[
    *author = [ David Legrand <david.legrand@clever-cloud.com> ]
    *date = [ 24 Apr 2023 ]
    *token = security
    *description = [ CVE-2024-{27280,27281,27282} ]
]]

dev-libs/libpcap[<1.10.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Sep 2024 ]
    token = security
    description = [ CVE-2023-7256, CVE-2024-8006 ]
]]

net-dns/libidn2[<2.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Oct 2019 ]
    token = security
    description = [ CVE-2019-12290 ]
]]

app-arch/cpio[<2.13] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Nov 2019 ]
    token = security
    description = [ CVE-2015-1197, CVE-2016-2037, CVE-2019-14866 ]
]]

dev-libs/fribidi[<1.0.7-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 09 Nov 2019 ]
    token = security
    description = [ CVE-2019-18397 ]
]]

dev-libs/oniguruma[<6.9.5_p1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Oct 2020 ]
    token = security
    description = [ CVE-2020-26159 ]
]]

app-shells/zsh[<5.8.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Feb 2022 ]
    token = security
    description = [ CVE-2021-45444 ]
]]

dev-libs/glib-networking[<2.64.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jul 2020 ]
    token = security
    description = [ CVE-2020-13645 ]
]]

media-libs/libjpeg-turbo[<2.0.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Jul 2020 ]
    token = security
    description = [ CVE-2020-13790 ]
]]

sys-fs/cryptsetup[<2.4.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Jan 2022 ]
    token = security
    description = [ CVE-2021-4122 ]
]]

(
    virtual/jdk:22
    virtual/jre:22
    virtual/jdk:23
    virtual/jre:23
) [[
    *author = [ David Legrand <david.legrand@clever-cloud.com> ]
    *date = [ 20 sep 2024 ]
    *token = testing
    *description = [ Mask non-LTS releases, most software is only tested on LTS ]
]]

mail-filter/procmail[<3.22-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Sep 2020 ]
    token = security
    description = [ CVE-2014-3618, CVE-2017-16844 ]
]]

media-libs/jpeg[<9d] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Oct 2020 ]
    token = security
    description = [ CVE-2020-14152, CVE-2020-14153 ]
]]

dev-scm/gitolite[<3.6.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Oct 2020 ]
    token = security
    description = [ CVE-2018-20683 ]
]]

sys-apps/sysstat[<12.6.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Feb 2023 ]
    token = security
    description = [ CVE-2022-39377 ]
]]

mail-client/mutt[<2.2.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Sep 2023 ]
    token = security
    description = [ CVE-2023-4874, CVE-2023-4875 ]
]]

sys-libs/musl[<1.1.24-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 22 Nov 2020 ]
    token = security
    description = [ CVE-2020-28928 ]
]]

sys-libs/musl[>=1.2.0&<1.2.1-r1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 22 Nov 2020 ]
    token = security
    description = [ CVE-2020-28928 ]
]]

dev-libs/openssl[<1.1.1w] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Sep 2023 ]
    token = security
    description = [ CVE-2023-4807 ]
]]

dev-libs/openssl[<3.3.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Sep 2024 ]
    token = security
    description = [ CVE-2024-5535, CVE-2024-6119 ]
]]

dev-libs/p11-kit[<0.23.22] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 14 Jan 2021 ]
    token = security
    description = [ CVE-2020-29361, CVE-2020-29362, CVE-2020-29363 ]
]]

sys-apps/gptfdisk[<1.0.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jan 2021 ]
    token = security
    description = [ CVE-2020-0256, CVE-2021-0308 ]
]]

dev-libs/crypto++[<8.6.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Sep 2021 ]
    token = security
    description = [ CVE-2021-40530 ]
]]

sys-apps/sydbox[>=2.0.0] [[
    author = [ Ali Polatel <alip@chesswob.org> ]
    date = [ 10 Oct 2023 ]
    token = testing
    description = [ Paludis does not support Sydbox API 3 yet ]
]]

sys-apps/sydbox-bin [[
    author = [ Ali Polatel <alip@chesswob.org> ]
    date = [ 24 Nov 2023 ]
    token = testing
    description = [ Paludis does not support Sydbox API 3 yet ]
]]

dev-libs/libuv[<1.41.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jul 2021 ]
    token = security
    description = [ CVE-2021-22918 ]
]]

net-www/lynx[<2.9.0_pre9] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Aug 2021 ]
    token = security
    description = [ CVE-2021-38165 ]
]]

app-spell/aspell[<0.60.8-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 28 Aug 2021 ]
    token = security
    description = [ CVE-2019-25051 ]
]]

app-arch/lz4[<1.9.3-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Sep 2021 ]
    token = security
    description = [ CVE-2021-3520 ]
]]

dev-lang/tcl[<8.6.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Dec 2021 ]
    token = security
    description = [ CVE-2021-35331 ]
]]

app-arch/unshield[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Jan 2022 ]
    token = security
    description = [ CVE-2015-1386 ]
]]

dev-libs/mbedtls[<3.6.1] [[
    author = [ Ridai Govinda Pombo <beholders.eye@disroot.org> ]
    date = [ 05 Sep 2024 ]
    token = security
    description = [ CVE-2024-45157, CVE-2024-45158, CVE-2024-45159 ]
]]

dev-util/ragel[>=7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Mar 2022 ]
    token = testing
    description = [ Development version ]
]]

sys-libs/zlib[<1.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Jan 2024 ]
    token = security
    description = [ CVE-2023-45853 ]
]]

app-arch/gzip[<1.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Apr 2022 ]
    token = security
    description = [ ZDI-CAN-16587 ]
]]

app-arch/xz[>=5.6.0&<5.6.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Mar 2024 ]
    token = security
    description = [ CVE-2024-3094 ]
]]

dev-util/pkg-config [[
    author = [ Marvin Schmidt <marv@exherbo.org> ]
    date = [ 12 Sep 2022 ]
    token = pending-removal
    description = [ Hasn't seen a release in years and problems accumulate ]
]]

sys-fs/squashfs-tools[<4.5.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Nov 2022 ]
    token = security
    description = [ CVE-2021-41072 ]
]]

dev-util/pkgconf[<1.9.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Jan 2023 ]
    token = security
    description = [ CVE-2023-24056 ]
]]

dev-libs/apr[<1.7.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Jan 2023 ]
    token = security
    description = [ CVE-2021-35940, CVE-2022-24963 ]
]]

dev-libs/apr-util[<1.6.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Jan 2023 ]
    token = security
    description = [ CVE-2022-25147 ]
]]

(
    dev-libs/libressl[>=3.5&<3.5.4]
    dev-libs/libressl[>=3.6&<3.6.2]
) [[
    *author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    *date = [ 10 Feb 2023 ]
    *token = security
    *description = [ Security vulnerability, details in the release notes ]
]]

sys-fs/multipath-tools[<0.9.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Feb 2023 ]
    token = security
    description = [ CVE-2022-41973, CVE-2022-41974 ]
]]

net-mail/fetchmail[<6.4.36] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Feb 2023 ]
    token = security
    description = [ CVE-2021-39272 ]
]]

sys-apps/lesspipe[<2.07] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Mar 2023 ]
    token = security
    description = [ CVE-2022-44542 ]
]]

dev-libs/libressl[<3.8] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 19 Apr 2024 ]
    token = security
    description = [ EOL branch (3.7 since 2024-04-10), no security updates ]
]]

dev-libs/botan[>=3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 May 2023 ]
    token = testing
    description = [ Not all dependents are ready ]
]]

sys-libs/libcap[<2.69] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 May 2023 ]
    token = security
    description = [ CVE-2023-2602, CVE-2023-2603 ]
]]

net-libs/nghttp2[<1.55.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Aug 2023 ]
    token = security
    description = [ CVE-2023-35945 ]
]]

app-editors/emacs[<29.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Nov 2023 ]
    token = security
    description = [ CVE-2023-28617 ]
]]

(
    dev-libs/libxml2:2.0[>=2.12.0]
    dev-python/libxml2:0[>=2.12.0]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 17 Nov 2023 ]
    *token = testing
    *description = [ Breaks building a number of dependants ]
]]

dev-libs/libfastjson[<1.2304.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Dec 2023 ]
    token = security
    description = [ CVE-2020-12762 ]
]]

sys-apps/rsyslog[<8.2310.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Dec 2023 ]
    token = security
    description = [ CVE-2022-24903 ]
]]

(
    mail-mta/postfix[>=3.7&<3.7.9]
    mail-mta/postfix[>=3.8&<3.8.4]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 27 Dec 2023 ]
    *token = security
    *description = [ CVE-2023-51764, https://www.postfix.org/smtp-smuggling.html ]
]]

dev-libs/cjson[<1.7.18] [[
    author = [ David Legrand <david.legrand@clever-cloud.com> ]
    date = [ 24 may 2024 ]
    token = security
    description = [ CVE-2024-31755 ]
]]

(
    dev-lang/erlang[<24.3.4.15]
    dev-lang/erlang:25[<25.3.2.8]
    dev-lang/erlang:26[<26.2.1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 04 Jan 2024 ]
    *token = security
    *description = [ CVE-2023-48795 ]
]]

mail-mta/sendmail[<8.18.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Feb 2024 ]
    token = security
    description = [ CVE-2023-51765 ]
]]

(
    sys-devel/gcc:14
    sys-libs/libatomic:14
    sys-libs/libgcc:14
    sys-libs/libgfortran:14
    sys-libs/libgomp:14
    sys-libs/libquadmath:14
    sys-libs/libsanitizer:14
    sys-libs/libstdc++:14
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 05 Feb 2024 ]
    *token = testing
    *description = [ Packages will fail to build and needs testing besides that ]
]]

dev-scm/gitolite [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 24 Apr 2024 ]
    token = pending-removal
    description = [ Please speak up if you still need this, we don't use it
                    anymore for our infra ]
]]

dev-libs/boost[~>1.86.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Aug 2024 ]
    token = testing
    description = [ Likely to break some packages (e.g. wesnoth) ]
]]

dev-libs/icu:75.1 [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 Apr 2024 ]
    token = testing
    description = [ Breaks at least some by packages by pulling in C++17 ]
]]

dev-libs/iniparser[<4.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Jun 2024 ]
    token = security
    description = [ CVE-2023-33461 ]
]]

dev-libs/orc[<0.4.39] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Jul 2024 ]
    token = security
    description = [ CVE-2024-40897 ]
]]

dev-python/setuptools[>=72.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Aug 2024 ]
    token = testing
    description = [ Removed the deprecated setup.py test command ]
]]
