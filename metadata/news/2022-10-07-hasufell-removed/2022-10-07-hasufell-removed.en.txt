Title: repository/hasufell has been removed from ::unavailable-unofficial
Author: Heiko Becker
Content-Type: text/plain
Posted: 2022-10-07
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: repository/hasufell

::hasufell [1] has been removed from ::unavailable-unofficial since it is
not maintained anymore.

If you use any packages from ::hasufell, you can resurrect them at any time
by following the instructions on our contributing [2] doc and maintain them
in your own repository. Some there already moved to new repositories.

To check if you have any packages from this repo installed, you can use

# cave print-ids -m '*/*::hasufell->installed'

If there are no packages left, which haven't found a new home in another
repository, it's safe to just remove the repo's config file as well as its
directory.

[1]: https://gogs.hasufell.de/hasufell/hasufell-repository
[2]: https://exherbo.org/docs/contributing.html
