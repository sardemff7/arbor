Title: app-terminal/zoxide::danyspin97 moved to sys-apps/zoxide::rust
Author: Maxime Sorin <msorin@msorin.com>
Content-Type: text/plain
Posted: 2021-08-12
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: app-terminal/zoxide

zoxide, formerly known as app-terminal/zoxide::danyspin97, has been moved to sys-apps/zoxide::rust.

Please install sys-apps/zoxide and *afterwards* uninstall app-terminal/zoxide.

1. Take note of any packages depending on app-terminal/zoxide:
cave resolve \!app-terminal/zoxide

2. Install sys-apps/zoxide:
cave resolve sys-apps/zoxide -zx

3. Re-install the packages from step 1.

4. Uninstall app-terminal/zoxide
cave resolve \!app-terminal/zoxide -x

Do it in *this* order or you'll potentially *break* your system.
