Title: multible binaries has been removed from dev-db/postgresql-client
Author: Thomas Berger <loki@lokis-chaos.de>
Content-Type: text/plain
Posted: 2018-11-15
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-db/postgresql-client

dev-db/postgresql-client installed mutliple binaries that are documented
as "client applications" but may be version dependent.

The binaries listed below has been moved to the server package and can be found
under /usr/host/libexec/postgresql-${SLOT}


- pg_isready
- pg_basebackup
- pg_receivewal
- pg_recvlogical
- pg_dump
- pg_restore
- pg_dumpall

While using these binaries with older versions of PostgreSQL server may work,
it can result in unintended behavior.

For example, dumping a PostgreSQL 9.6 database with pg_dumpall Version 11 works,
but importing the resulting dump back into a PostgreSQL 9.6 database may need
manual editing of the dump file, as PostgreSQL 11 specific syntax may be used.

To avoid confusion and broken systems these binaries are no longer provided by
dev-db/postgresql-client.

If pg_dump{,all} is needed in a serverless environment (for example for analytics),
these can be pulled in by the `pgdump` option for dev-db/postgresql-client.

