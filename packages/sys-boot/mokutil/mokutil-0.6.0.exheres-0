# Copyright 2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lcp ] \
    bash-completion \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="The utility to manipulate machine owner keys"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( libc: musl )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-apps/keyutils[>=1.5]
        sys-boot/efivar[>=0.12]
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=0.9.8] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Use-PKG_PROG_PKG_CONFIG-macro-from-pkg.m4-to-detect-.patch
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "bash-completion bash-completion-dir ${BASHCOMPLETIONDIR}"
)

src_install() {
    default

    bash-completion_src_install
}

