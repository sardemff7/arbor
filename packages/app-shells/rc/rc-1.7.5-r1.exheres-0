# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY=https://gitlab.exherbo.org/hex/${PN}
SCM_TAG=v${PV}
require scm-git

SUMMARY="Plan9 RC shell (reimplemented for Unix by Byron Rakitzis)"
HOMEPAGE="https://github.com/rakitzis/${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="libedit readline static
    ( libedit readline static ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/make
    build+run:
        libedit? ( dev-libs/libedit )
        readline? ( sys-libs/readline:= )
"

rc_edit() {
    # Pick line editing library.
    if option libedit; then
        echo edit
    elif option readline; then
        echo readline
    else
        echo null
    fi
}

src_compile() {
    # Handle static option
    local ldlibs
    if option static; then
        ldlibs=-static
        CFLAGS+=' -static'
        LDFLAGS+=' -static'
    fi

    emake V=1 EDIT=$(rc_edit) LDLIBS=$ldlibs YACC='bison -y'
}

src_test() {
    emake V=1 EDIT=$(rc_edit) check
}

src_install() {
    emake -j1 \
        V=1 \
        EDIT=$(rc_edit) \
        DESTDIR="${IMAGE}" \
        PREFIX=/usr/$(exhost --target) \
        MANPREFIX=/usr/share/man \
        install
    emagicdocs
}
