# Copyright 2008 Santiago M. Mola
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libxslt-1.1.23.ebuild', which is:
#   Copyright 1999-2008 Gentoo Foundation

require gnome.org [ suffix=tar.xz ]
# configure: No package 'python-3.1' found
# can be removed once upstream rebuilt using automake >= 1.16.3
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require python [ with_opt=true blacklist=none multibuild=false ]

SUMMARY="XSLT libraries and tools"
HOMEPAGE="https://gitlab.gnome.org/GNOME/${PN}/-/wikis/home"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="crypt debug doc examples"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libxml2:2.0[>=2.6.27]
        crypt? ( dev-libs/libgcrypt[>=1.1.92] )
        python? ( dev-python/libxml2[python_abis:*(-)?] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-tests-Fix-build-with-older-libxml2.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    # Always pass --with-debugger. It is required by third parties (see
    # e.g. Gentoo bug #98345)
    --with-debugger
    --with-plugins
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "crypt crypto"
    "debug"
    "debug mem-debug"
    "python"
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( FEATURES )

src_prepare() {
    default

    # Fails for unknown reasons with musl
    [[ $(exhost --target) == *-musl* ]] && edo rm tests/REC/test-10-3.xsl
}

src_configure() {
    # Use the target's libgcrypt-config. This is safe because libgcrypt-config is a standalone shell
    # script with /bin/sh as shebang.
    LIBGCRYPT_CONFIG=/usr/$(exhost --target)/bin/libgcrypt-config \
        default
}

src_install() {
    default

    keepdir /usr/$(exhost --target)/lib/${PN}-plugins

    if ! option doc; then
        edo rm -r "${IMAGE}"/usr/share/gtk-doc
    fi

    if ! option examples; then
        edo rm -rf "${IMAGE}"/usr/share/doc/${PNVR}/{python,tutorial{,2}}
    fi
}

