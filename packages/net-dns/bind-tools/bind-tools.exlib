# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic

MY_PV=${PV/_p/-P}
MY_PNV=${PN//-tools}-${MY_PV}

export_exlib_phases src_configure src_compile src_install

SUMMARY="Tools from the bind package: dig, dnssec-keygen, host, nslookup, nsupdate"
DESCRIPTION="
DNS tools like dig and nslookup are extremely useful to have. They usually ship
with bind itself but it doesn't make any sense to install the full bind package
for them. Thus, this allows you to install just the tools.
"
HOMEPAGE="https://www.isc.org/software/bind"
DOWNLOADS="https://downloads.isc.org/isc/bind${PV%%.*}/${MY_PV}/${MY_PNV}.tar.xz"

LICENCES="MPL-2.0"
SLOT="0"
MYOPTIONS="
    geoip [[ description = [ ACLs can also be used for geographic access restrictions. ] ]]
    idn
    kerberos
    openssl-compat [[ description = [ Enable openssl-1.1 engines when using openssl-3 ] ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Tons of sandbox violations.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/jemalloc
        dev-libs/libuv[>=1.37.0]
        dev-libs/libxml2:2.0[>=2.6.0]
        net-libs/nghttp2[>=1.6.0]
        sys-libs/readline:=
        geoip? ( net-libs/libmaxminddb )
        idn? ( net-dns/libidn2:= )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        providers:libressl? ( dev-libs/libressl:=[>=2.7.0] )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --libdir=/usr/$(exhost --target)/lib/${PN}
    # This could be optional, but fails to build if disabled, probably fixed in
    # main, but nice for privacy and nghttp2 is small so hard-enable it for
    # now.
    --enable-doh
    --disable-dnstap
    --disable-fips-mode
    # Only used by named (net-dns/bind)
    --disable-linux-caps
    --disable-static
    --with-jemalloc
    --with-libxml2
    --with-openssl=/usr/$(exhost --target)
    --with-readline=readline
    --with-zlib
    --without-json-c
    --without-lmdb
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    geoip
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'geoip maxminddb'
    'idn libidn2'
    'kerberos gssapi'
)

WORK=${WORKBASE}/${MY_PNV}

BIND_TOOLS_LIBS=(
    isc
    dns
    isccfg
    ns
    irs
    bind9
)
BIND_TOOLS_BINS=(
    delv
    dig
    dnssec
    nsupdate
)

BIND_TOOLS_dnssec_PARAMS="bin_PROGRAMS=dnssec-keygen"

bind-tools-subdirs-emake() {
    local subdir
    local params_var
    for subdir in "${BIND_TOOLS_LIBS[@]/#/lib/}"; do
        params_var=BIND_TOOLS_${subdir#lib/}_PARAMS
        emake -C "${subdir}" "${@}" ${!params_var}
    done
    for subdir in "${BIND_TOOLS_BINS[@]/#/bin/}"; do
        params_var=BIND_TOOLS_${subdir#bin/}_PARAMS
        emake -C "${subdir}" "${@}" ${!params_var}
    done
}

bind-tools_src_configure() {
    option openssl-compat && append-flags -DOPENSSL_API_COMPAT=10100
    default
}

bind-tools_src_compile() {
    emake SUBDIRS=
    bind-tools-subdirs-emake

    emake -C doc/man
}

bind-tools_src_install() {
    bind-tools-subdirs-emake -j1 DESTDIR="${IMAGE}" install
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/include

    doman doc/man/delv.1
    doman doc/man/{dig.1,host.1,nslookup.1}
    doman doc/man/dnssec-keygen.1
    doman doc/man/nsupdate.1

    emagicdocs
}

