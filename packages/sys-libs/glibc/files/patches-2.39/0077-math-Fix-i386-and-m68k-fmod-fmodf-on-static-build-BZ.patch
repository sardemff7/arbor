Upstream: yes, taken from release/2.39/master

From 6cb25aff8583421cf7a55ddd40abd7bd49176c60 Mon Sep 17 00:00:00 2001
From: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date: Thu, 14 Mar 2024 14:59:35 -0300
Subject: [PATCH 77/80] math: Fix i386 and m68k fmod/fmodf on static build (BZ
 31488)

The commit 16439f419b removed the static fmod/fmodf on i386 and m68k
with and empty w_fmod.c (required for the ABIs that uses the newly
implementation).  This patch fixes by adding the required symbols on
the arch-specific w_fmod{f}_compat.c implementation.

To statically build fmod fails on some ABI (alpha, s390, sparc) because
it does not export the ldexpf128, this is also fixed by this patch.

Checked on i686-linux-gnu and with a build for m68k-linux-gnu.

Reviewed-by: Aurelien Jarno <aurelien@aurel32.net>
Tested-by: Aurelien Jarno <aurelien@aurel32.net>
(cherry picked from commit 0b716305dfb48c2d13ed4f7d06c082b90c1d226f)
---
 sysdeps/i386/fpu/w_fmod_compat.c         | 7 ++++---
 sysdeps/i386/fpu/w_fmodf_compat.c        | 7 ++++---
 sysdeps/ieee754/ldbl-opt/s_ldexpl.c      | 4 ++--
 sysdeps/m68k/m680x0/fpu/w_fmod_compat.c  | 5 +++--
 sysdeps/m68k/m680x0/fpu/w_fmodf_compat.c | 7 ++++---
 5 files changed, 17 insertions(+), 13 deletions(-)

diff --git a/sysdeps/i386/fpu/w_fmod_compat.c b/sysdeps/i386/fpu/w_fmod_compat.c
index 5ac9995ffd..528bfc2a13 100644
--- a/sysdeps/i386/fpu/w_fmod_compat.c
+++ b/sysdeps/i386/fpu/w_fmod_compat.c
@@ -7,8 +7,9 @@
 # define LIBM_SVID_COMPAT 1
 # undef compat_symbol
 # define compat_symbol(a, b, c, d)
-#endif
-#include <math/w_fmod_compat.c>
-#ifdef SHARED
+# include <math/w_fmod_compat.c>
 libm_alias_double (__fmod_compat, fmod)
+#else
+#include <math-type-macros-double.h>
+#include <w_fmod_template.c>
 #endif
diff --git a/sysdeps/i386/fpu/w_fmodf_compat.c b/sysdeps/i386/fpu/w_fmodf_compat.c
index cc417e07d3..5a61693e51 100644
--- a/sysdeps/i386/fpu/w_fmodf_compat.c
+++ b/sysdeps/i386/fpu/w_fmodf_compat.c
@@ -7,8 +7,9 @@
 # define LIBM_SVID_COMPAT 1
 # undef compat_symbol
 # define compat_symbol(a, b, c, d)
-#endif
-#include <math/w_fmodf_compat.c>
-#ifdef SHARED
+# include <math/w_fmodf_compat.c>
 libm_alias_float (__fmod_compat, fmod)
+#else
+#include <math-type-macros-float.h>
+#include <w_fmod_template.c>
 #endif
diff --git a/sysdeps/ieee754/ldbl-opt/s_ldexpl.c b/sysdeps/ieee754/ldbl-opt/s_ldexpl.c
index 1afbe7d8ad..932cc4341c 100644
--- a/sysdeps/ieee754/ldbl-opt/s_ldexpl.c
+++ b/sysdeps/ieee754/ldbl-opt/s_ldexpl.c
@@ -17,13 +17,13 @@
    License along with the GNU C Library; if not, see
    <https://www.gnu.org/licenses/>.  */
 
-#if IS_IN (libc)
+#if IS_IN (libc) && defined SHARED
 # define declare_mgen_alias(f,t)
 #endif
 #include <math-type-macros-ldouble.h>
 #include <s_ldexp_template.c>
 
-#if IS_IN (libc)
+#if IS_IN (libc) && defined SHARED
 long_double_symbol (libc, __ldexpl, ldexpl);
 long_double_symbol (libc, __wrap_scalbnl, scalbnl);
 #endif
diff --git a/sysdeps/m68k/m680x0/fpu/w_fmod_compat.c b/sysdeps/m68k/m680x0/fpu/w_fmod_compat.c
index 527d4fbed2..57f38091e6 100644
--- a/sysdeps/m68k/m680x0/fpu/w_fmod_compat.c
+++ b/sysdeps/m68k/m680x0/fpu/w_fmod_compat.c
@@ -7,8 +7,9 @@
 # define LIBM_SVID_COMPAT 1
 # undef compat_symbol
 # define compat_symbol(a, b, c, d)
-#endif
 #include <math/w_fmod_compat.c>
-#ifdef SHARED
 libm_alias_double (__fmod_compat, fmod)
+#else
+#include <math-type-macros-double.h>
+#include <w_fmod_template.c>
 #endif
diff --git a/sysdeps/m68k/m680x0/fpu/w_fmodf_compat.c b/sysdeps/m68k/m680x0/fpu/w_fmodf_compat.c
index 5043586b91..88db07f443 100644
--- a/sysdeps/m68k/m680x0/fpu/w_fmodf_compat.c
+++ b/sysdeps/m68k/m680x0/fpu/w_fmodf_compat.c
@@ -7,8 +7,9 @@
 # define LIBM_SVID_COMPAT 1
 # undef compat_symbol
 # define compat_symbol(a, b, c, d)
-#endif
-#include <math/w_fmodf_compat.c>
-#ifdef SHARED
+# include <math/w_fmodf_compat.c>
 libm_alias_float (__fmod_compat, fmod)
+#else
+#include <math-type-macros-float.h>
+#include <w_fmod_template.c>
 #endif
-- 
2.45.2

