# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ pn="libunwind" check_target="check-unwind" asserts=true rtlib=true ]

export_exlib_phases src_prepare src_install

SUMMARY="LLVM implementation of C++ runtime stack unwinder"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !dev-libs/libunwind [[
            description = [ Both packages provide a libunwind implementation ]
            resolution = uninstall-blocked-after
        ]]
"

CMAKE_SOURCE="${WORKBASE}/llvm-project/runtimes"

llvm-libunwind_src_prepare() {
    cd "${WORKBASE}"/llvm-project
    default
}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_CXX_COMPILER_TARGET:STRING="$(exhost --target)"

    -DLLVM_ENABLE_RUNTIMES:STRING="libunwind"

    -DLLVM_EXTERNAL_LIT:PATH="${WORKBASE}"/llvm-project/llvm/utils/lit/lit.py

    -DLIBUNWIND_ENABLE_CROSS_UNWINDING:BOOL=ON
    -DLIBUNWIND_ENABLE_SHARED:BOOL=ON
    -DLIBUNWIND_ENABLE_STATIC:BOOL=ON
    -DLIBUNWIND_ENABLE_THREADS:BOOL=ON
    -DLIBUNWIND_ENABLE_WERROR:BOOL=OFF
    -DLIBUNWIND_INSTALL_LIBRARY:BOOL=ON
    -DLIBUNWIND_INSTALL_SHARED_LIBRARY:BOOL=ON
    -DLIBUNWIND_INSTALL_STATIC_LIBRARY:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'asserts LIBUNWIND_ENABLE_ASSERTIONS'
    'providers:compiler-rt LIBUNWIND_USE_COMPILER_RT'
)

# FIXME: tech where lit is
RESTRICT="test"

llvm-libunwind_src_install() {
    DESTDIR="${IMAGE}" eninja install-unwind
}

