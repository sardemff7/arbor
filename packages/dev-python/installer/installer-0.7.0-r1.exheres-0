# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi python [ blacklist=2 ]
# prevent circular dependencies and avoid bootstrap issues
# due to py-pep517.exlib having a dependency on installer
# by using the pre-built wheel
#require pypi py-pep517 [ backend=flit_core test=pytest ]

SUMMARY="A library for installing Python wheels"
DOWNLOADS+=" https://files.pythonhosted.org/packages/e5/ca/1172b6638d52f2d6caa2dd262ec4c811ba59eee96d54a7701930726bce18/${PNV}-py3-none-any.whl"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES=""
#   suggestion:
#       dev-python/pytest-cov[python_abis:*(-)?]
#       dev-python/pytest-xdist[python_abis:*(-)?]
#

#test_one_multibuild() {
#   # Avoid cyclic dependencies when bootstrapping
#   if has_version dev-python/pytest[python_abis:$(python_get_abi)] &&
#      has_version dev-python/pytest-cov[python_abis:$(python_get_abi)] &&
#      has_version dev-python/pytest-xdist[python_abis:$(python_get_abi)] ; then
#       py-pep517_run_tests_pytest
#   else
#       ewarn "One or more test dependencies not yet installed, skipping tests"
#   fi
#

configure_one_multibuild() {
    :
}

compile_one_multibuild() {
    :
}

install_one_multibuild() {
    PYTHONPATH="${FETCHEDDIR}"/${PNV}-py3-none-any.whl edo ${PYTHON} -m installer \
        --destdir "${IMAGE}" \
        --prefix /usr/$(exhost --target) \
        "${FETCHEDDIR}"/${PNV}-py3-none-any.whl

    # Remove windows executables
    edo rm "${IMAGE}"/$(python_get_sitedir)/${PN}/_scripts/*.exe

    emagicdocs
}

