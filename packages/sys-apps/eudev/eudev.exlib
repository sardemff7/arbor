# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Copyright 2023, 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2
#
# Parts derived from "systemd.exlib" from ::arbor, which is:
#   Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
#   Distributed under the terms of the GNU General Public License v2
#
# Specifically, the commands in pkg_setup, pkg_postinst, and src_install.
#

require github [ user='eudev-project' tag="v${PV}" ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 ] ]
require udev-rules
require openrc-service [ openrc_initd_files=[ "${WORKBASE}/udev-gentoo-scripts-${OPENRC_SCRIPTS_VER}/init.d" "${FILES}"/openrc/initd ] openrc_confd_files=[ "${WORKBASE}/udev-gentoo-scripts-${OPENRC_SCRIPTS_VER}/conf.d" ] ]
require s6-rc-service

export_exlib_phases pkg_setup src_install src_unpack pkg_postinst

DOWNLOADS+="
    parts:openrc? (
        https://gitweb.gentoo.org/proj/udev-gentoo-scripts.git/snapshot/udev-gentoo-scripts-${OPENRC_SCRIPTS_VER}.tar.bz2
    )
"

SUMMARY="A device manager for the Linux kernel; fork of systemd-udev"
SLOT="0"
LICENCES="GPL-2"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-kernel/linux-headers[>=2.6.39]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-util/gperf
        sys-apps/kmod[>=15]
    run:
        !sys-apps/systemd [[
            description = [ ${CATEGORY}/${PN} is a udev daemon which provides the same library and programs that systemd-udev does ]
            resolution = manual
        ]]
    recommendation:
        sys-kernel/linux-headers[>=3.8] [[ note = [ Support for in-kernel firmware loading ] ]]
"

MYOPTIONS="gtk-doc"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-udev-Makefile.am-change-udev.pc-to-pkgconfiglib_DATA.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-blkid
    --enable-hwdb
    # eudev's gudev implementation is disabled, and will be removed
    # in favor of gnome-desktop/libgudev in later versions.
    # https://bugs.gentoo.org/show_bug.cgi?id=552036#c2
    --enable-introspection=no
    --enable-manpages
    --disable-gudev
    --disable-selinux
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
)

eudev_pkg_setup() {
    exdirectory --allow /etc/udev/rules.d
}

eudev_src_unpack() {
    # Note(alip): We need default_src_unpack for openrc init scripts.
    ever is_scm && scm_src_unpack
    default_src_unpack
}

eudev_src_install() {
    default

    # Keep the udev rules, hwdb, etc.
    keepdir /usr/$(exhost --target)/lib/udev/devices
    keepdir /etc/udev/hwdb.d
    keepdir /etc/udev/rules.d

    # Add rules to the CONFIG_PROTECT mask
    hereenvd 20udev <<EOF
CONFIG_PROTECT_MASK="${UDEVRULESDIR}"
EOF

    install_openrc_files
    install_s6-rc_files
}

eudev_pkg_postinst() {
    default

    if exhost --is-native -q && [[ ${ROOT} == / ]]; then
        # Note(alip): The chroot test will not work under syd-3,
        # due to procfs hardening. To workaround this, we create
        # a script and make the check outside the sandbox.
        # Note(alip): This method should work for syd-{1,3} and
        # we do not support syd-0 any longer.
        script="${TEMP}/restart.bash"
        signal="${TEMP}/restart.done"

        cat >"${script}" <<EOF
#!/bin/bash -x
# Restart the udev daemon if we are native, and running in the root it's installed to
if [[ -r /proc/1/root && /proc/1/root -ef /proc/self/root/ ]]; then
    pkill udevd
    sleep 1
    pkill -9 udevd

    /usr/$(exhost --target)/bin/udevd --daemon || echo >&2 "udevd couldn't be restarted"
fi
touch "${signal}"
EOF
        edo chmod +x "${script}"

        # Two things to note:
        # 1. esandbox exec executes process in the background.
        # 2. esandbox exec executes process with CWD set to /.
        # TODO: Document these in Exheres for Smarties.
        esandbox exec "${script}"
        # Let's wait for a minute for the restart to complete.
        for _ in {1..60}; do
            test -e "${signal}" && break
            sleep 1
        done
    fi
}

