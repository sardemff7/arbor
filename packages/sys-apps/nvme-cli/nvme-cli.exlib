# Copyright 2024 David Legrand <david.legrand@clever-cloud.com>
# Copyright 2017 Bernhard Frauendienst
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion
require github [ user=linux-nvme tag=v${PV} ]
require meson [ cross_prefix=true ]

SUMMARY="NVMe management command line interface"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-text/asciidoctor
            app-text/docbook-xsl-ns-stylesheets
            app-text/xmlto
        )
    build+run:
        dev-libs/json-c:=[>=0.13]
        sys-libs/libnvme[>=1.10]
"

MESON_SRC_CONFIGURE_PARAMS=( -Djson-c=enabled )
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docs-build'
    'doc docs all false'
    "doc htmldir /usr/share/doc/${PNVR} ''"
)

src_install() {
    meson_src_install
    option bash-completion || edo rm -r "${IMAGE}"/usr/share/bash-completion/
    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh/
}

