# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=xtensor-stack ] cmake

SUMMARY="C++ wrappers for SIMD intrinsics and mathematical functions"
DESCRIPTION="
SIMD (Single Instruction, Multiple Data) is a feature of microprocessors that
has been available for many years. SIMD instructions perform a single
operation on a batch of values at once, and thus provide a way to
significantly accelerate code execution. However, these instructions differ
between microprocessor vendors and compilers.
xsimd provides a unified means for using these features for library authors.
Namely, it enables manipulation of batches of numbers with the same arithmetic
operators as for single values. It also provides accelerated implementation of
common mathematical functions operating on batches."

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-util/doctest[>=2.4.9]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Would need https://github.com/xtensor-stack/xtl
    -DENABLE_XTL_COMPLEX:BOOL=FALSE
    -DXSIMD_DOWNLOAD_DOCTEST:BOOL=FALSE
    -DXSIMD_ENABLE_WERROR:BOOL=FALSE
    -DXSIMD_SKIP_INSTALL:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

