Source/Upstream: Yes, but only for php >= 8.2.x
Reason: Fix build with icu 75.1

From 4e21a26db24cfeb0c8a3ba43dba2d7d5599d2ee5 Mon Sep 17 00:00:00 2001
From: Calvin Buckley <calvin@cmpct.info>
Date: Fri, 10 May 2024 14:20:17 -0300
Subject: [PATCH] Fix check for newer versions of ICU (#14186)

* Fix check for newer versions of ICU

The previous test would always trigger, even if the version of ICU
installed didn't require C++17. This was because it incorrectly used
the `test` program, which broke the build on systems without a C++17
compiler.

Tested with macOS 14 and i 7.2.

* Fix broken ICU version check for definition

Same as the previous fix for C++17.

---------

Co-authored-by: Peter Kokot <peterkokot@gmail.com>
---
 build/php.m4       | 5 ++---
 ext/intl/config.m4 | 2 +-
 2 files changed, 3 insertions(+), 4 deletions(-)

diff --git a/build/php.m4 b/build/php.m4
index 2bc0733a978a6..8ce9656ad1d89 100644
--- a/build/php.m4
+++ b/build/php.m4
@@ -1929,9 +1929,8 @@ AC_DEFUN([PHP_SETUP_ICU],[
   ICU_CFLAGS="$ICU_CFLAGS -DU_NO_DEFAULT_INCLUDE_UTF_HEADERS=1"
   ICU_CXXFLAGS="$ICU_CXXFLAGS -DUNISTR_FROM_CHAR_EXPLICIT=explicit -DUNISTR_FROM_STRING_EXPLICIT=explicit"
 
-  if test "$PKG_CONFIG icu-io --atleast-version=60"; then
-    ICU_CFLAGS="$ICU_CFLAGS -DU_HIDE_OBSOLETE_UTF_OLD_H=1"
-  fi
+  AS_IF([$PKG_CONFIG icu-io --atleast-version=60],
+    [ICU_CFLAGS="$ICU_CFLAGS -DU_HIDE_OBSOLETE_UTF_OLD_H=1"])
 ])
 
 dnl
diff --git a/ext/intl/config.m4 b/ext/intl/config.m4
index 48f5147ca7bbf..78396dc416926 100644
--- a/ext/intl/config.m4
+++ b/ext/intl/config.m4
@@ -82,7 +82,7 @@ if test "$PHP_INTL" != "no"; then
   PHP_REQUIRE_CXX()
 
   AC_MSG_CHECKING([if intl requires -std=gnu++17])
-  AS_IF([test "$PKG_CONFIG icu-uc --atleast-version=74"],[
+  AS_IF([$PKG_CONFIG icu-uc --atleast-version=74],[
     AC_MSG_RESULT([yes])
     PHP_CXX_COMPILE_STDCXX(17, mandatory, PHP_INTL_STDCXX)
   ],[
