# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ebiggers tag=v${PV} ]
require cmake

SUMMARY="Optimized library for DEFLATE/zlib/gzip compression and decompression"
DESCRIPTION="
libdeflate is a library for fast, whole-buffer DEFLATE-based compression and
decompression. The supported formats are:
* DEFLATE (raw)
* zlib (a.k.a. DEFLATE with a zlib wrapper)
* gzip (a.k.a. DEFLATE with a gzip wrapper)
libdeflate is heavily optimized. It is significantly faster than the zlib
library, both for compression and decompression, and especially on x86
processors. In addition, libdeflate provides optional high compression modes
that provide a better compression ratio than the zlib's \"level 9\".
libdeflate itself is a library, but the following command-line programs which
use this library are also provided:
* gzip (or gunzip), a program which mostly behaves like the standard
  equivalent, except that it does not yet have good streaming support and
  therefore does not yet support very large files
* benchmark, a program for benchmarking in-memory compression and decompression
"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DLIBDEFLATE_BUILD_STATIC_LIB:BOOL=FALSE
    -DLIBDEFLATE_FREESTANDING:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DLIBDEFLATE_BUILD_TESTS:BOOL=TRUE -DLIBDEFLATE_BUILD_TESTS:BOOL=FALSE'
)

